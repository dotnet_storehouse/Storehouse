﻿using Storebase.store;

namespace Storehouse {
    public class SimpleFileDiff {
        public string FileName;
        public FileData Old;
        public FileData New;
    }
}