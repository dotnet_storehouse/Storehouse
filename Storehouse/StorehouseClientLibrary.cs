﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Storebase.store;
using ConHost = Storebase.net.connectivity.ConHost;

namespace Storehouse {
    public static partial class StorehouseClientLibrary {
        private static StorehouseSettings _settings;

        public static List<StorageBucket> Buckets { get; private set; } = new List<StorageBucket>();

        public static StorageBucket ActiveBucket { get; private set; }

        public static StorageBucket LastFetchedBucket { get; private set; }

        public static NetworkMessageLoop NetML => NetworkMessageLoop.Instance;

        public static void Initialize(StorehouseSettings settings) {
            _settings = settings;

            var ser = new XmlSerializer(Buckets.GetType());
            if (File.Exists(_settings.BucketCacheFile)) {
                var strRead = File.OpenRead(_settings.BucketCacheFile);
                Buckets = (List<StorageBucket>) ser.Deserialize(strRead);
                strRead.Close();

                Buckets.ForEach(bucket => bucket.PostDeserialize());
            }
        }

        public static void SaveBuckets() {
            var ser = new XmlSerializer(Buckets.GetType());
            var strWrite = File.OpenWrite(_settings.BucketCacheFile);
            ser.Serialize(strWrite, Buckets);
            strWrite.Flush();
            strWrite.Close();
        }

        public static void Connect(string hostname, int port = 62822) {
            var c = ConHost.Connect(hostname, port, _settings.CertificatePath);
            NetworkMessageLoop.Instance = new NetworkMessageLoop(c);
            NetworkMessageLoop.Instance.Start();
        }
    }
}