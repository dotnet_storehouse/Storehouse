﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Storebase.net.@base;
using Storebase.store;
using Storehouse.err;

namespace Storehouse {
    public static partial class StorehouseClientLibrary {
        /// <summary>
        /// Retrieves the list of buckets present on the remote.
        /// </summary>
        /// <returns>The list of buckets present on the remote.</returns>
        public static List<StorageBucket> GetRemoteStorageBuckets() {
            var m = new Packet(Signal.Type.Control, Signal.Codes.BucketList).ToMsg();
            List<StorageBucket> lst = null;
            m.OnResponse += (_, msg) => {
                var str = new MemoryStream(msg.Response.Data);
                var ser = new XmlSerializer(typeof(List<StorageBucket>));
                lst = (List<StorageBucket>) ser.Deserialize(str);
            };
            NetML.PostMessage(m);

            m.ResponseHandle.WaitOne();

            return lst;
        }

        #region push

        /// <summary>
        /// Pushes the given bucket to the remote.
        /// Also, if the bucket is not added to the local listing yet, it can be added.
        /// </summary>
        /// <param name="bucket">The bucket to be pushed.</param>
        /// <param name="addIfNotListed">Whether or not to list the bucket if it isn't already. Default is false.</param>
        /// <exception cref="StorageBucketUnlistedException">Raised when <paramref name="addIfNotListed"/> is false and the bucket is not listed.</exception>
        /// <exception cref="DuplicateStorageBucketException">Raised, when the a bucket with the given <see cref="T:System.Guid"/> is already present on the remote</exception>
        public static void PushBucket(StorageBucket bucket, bool addIfNotListed = false) {
            if (addIfNotListed && !Buckets.Contains(bucket))
                Buckets.Add(bucket);
            else if (!Buckets.Contains(bucket)) throw new StorageBucketUnlistedException(bucket);

            var ser = new XmlSerializer(bucket.GetType());
            var str = new MemoryStream();
            ser.Serialize(str, bucket);
            var dat = (new[] {(byte) Signal.Codes.BucketPush}).Concat(str.ToArray()).ToArray();

            var m = new Packet(Signal.Type.Control, dat).ToMsg();
            NetML.PostMessage(m);

            m.ResponseHandle.WaitOne();

            switch ((Signal.Codes) m.Response.Data[0]) {
                case Signal.Codes.ErrorDuplicateBucket:
                    throw new DuplicateStorageBucketException(bucket.Guid.ToString());
            }
        }

        /// <summary>
        /// Pushes the given bucket to the remote.
        /// Also, if the bucket is not added to the local listing yet, it can be added.
        /// </summary>
        /// <param name="guid">The start of the buckets guid, formatted as returned by <see cref="T:System.Guid"/>.<see cref="M:System.Guid.ToString"/></param>
        /// <exception cref="DuplicateStorageBucketException">Raised, when the a bucket with the given <see cref="T:System.Guid"/> is already present on the remote</exception>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="AmbiguousGuidException">Raised when there are multiple buckets whose guid starts like <paramref name="guid"/>.</exception>
        public static void PushBucket(string guid) => PushBucket(GetBucketByGuid(guid));

        /// <summary>
        /// Tries to push the active bucket to the remote.
        /// </summary>
        /// <returns>Whether there was an active bucket that was pushed.</returns>
        /// <exception cref="DuplicateStorageBucketException">Raised, when the a bucket with the given <see cref="T:System.Guid"/> is already present on the remote</exception>
        public static bool PushBucket() {
            if (ActiveBucket != null) {
                PushBucket(ActiveBucket);
                return true;
            }

            return false;
        }

        #endregion

        #region fetch

        /// <summary>
        /// Fetches a bucket from the remote and assigns it to <see cref="LastFetchedBucket"/>.
        /// </summary>
        /// <param name="guid">The start of the buckets guid, formatted as returned by <see cref="T:System.Guid"/>.<see cref="M:System.Guid.ToString"/></param>
        /// <returns>The bucket that was fetched.</returns>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="AmbiguousGuidException">Raised when there are multiple buckets whose guid starts like <paramref name="guid"/>.</exception>
        public static StorageBucket FetchBucket(string guid) {
            var dat = new[] {(byte) Signal.Codes.BucketFetch};
            dat = dat.Concat(Encoding.Unicode.GetBytes(guid)).ToArray();

            var m = new Packet(Signal.Type.Control, dat).ToMsg();
            m.OnResponse += (_, msg) => {
                var data = msg.Response.Data;
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch ((Signal.Codes) data[0]) {
                    case Signal.Codes.Ack:
                        var ser = new XmlSerializer(typeof(StorageBucket));
                        var str = new MemoryStream(data.Skip(1).ToArray());
                        LastFetchedBucket = (StorageBucket) ser.Deserialize(str);
                        break;
                    case Signal.Codes.ErrorAmbiguousGuid:
                        throw new AmbiguousGuidException(guid);
                    case Signal.Codes.ErrorBucketNotFound:
                        throw new StorageBucketNotFoundException(guid);
                }
            };
            NetML.PostMessage(m);

            m.ResponseHandle.WaitOne();

            return LastFetchedBucket;
        }

        /// <summary>
        /// Fetches a bucket from the remote and assigns it to <see cref="LastFetchedBucket"/>.
        /// </summary>
        /// <param name="guid">The guid of the bucket.</param>
        /// <returns>The bucket that was fetched.</returns>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        public static StorageBucket FetchBucket(Guid guid) => FetchBucket(guid.ToString());

        #endregion

        #region pull

        /// <summary>
        /// Fetches and lists a bucket from the remote.
        /// </summary>
        /// <param name="guid">The start of the buckets guid, formatted as returned by <see cref="T:System.Guid"/>.<see cref="M:System.Guid.ToString"/></param>
        /// <param name="forceReFetch">When true, <see cref="FetchBucket(string)"/> will be called even when <see cref="LastFetchedBucket"/> has a matching guid.</param>
        /// <returns>The bucket that was pulled.</returns>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="AmbiguousGuidException">Raised when there are multiple buckets whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="DuplicateStorageBucketException">Raised when a bucket with the same guid as the fetched bucket is already listed.</exception>
        public static StorageBucket PullBucket(string guid, bool forceReFetch = false) {
            var b = LastFetchedBucket;
            if (forceReFetch || b != null && !b.Guid.ToString().StartsWith(guid)) b = FetchBucket(guid);

            AddBucket(b);
            return b;
        }

        /// <summary>
        /// Fetches and lists a bucket from the remote.
        /// </summary>
        /// <param name="guid">The guid of the bucket.</param>
        /// <param name="forceReFetch">When true, <see cref="FetchBucket(string)"/> will be called even when <see cref="LastFetchedBucket"/> has a matching guid.</param>
        /// <returns>The bucket that was pulled.</returns>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="DuplicateStorageBucketException">Raised when a bucket with the same guid as the fetched bucket is already listed.</exception>
        public static StorageBucket PullBucket(Guid guid, bool forceReFetch = false) =>
            PullBucket(guid.ToString(), forceReFetch);

        /// <summary>
        /// Tries to lists the <see cref="LastFetchedBucket"/>.
        /// </summary>
        /// <returns><see cref="LastFetchedBucket"/></returns>
        /// <exception cref="DuplicateStorageBucketException">Raised when a bucket with the same guid as the fetched bucket is already listed.</exception>
        public static StorageBucket PullBucket() {
            if (LastFetchedBucket != null) AddBucket(LastFetchedBucket);
            return LastFetchedBucket;
        }

        #endregion

        #region changes

        /// <summary>
        /// Gets the list of changed files from the remote and computes the differences.
        /// </summary>
        /// <param name="bucket">The bucket to compute the changes for.</param>
        /// <returns>The list of changes that have been found</returns>
        /// <exception cref="StorageBucketNotFoundException">If the bucket doesn't exist on the remote.</exception>
        public static List<SimpleFileDiff> GetSimpleChanges(StorageBucket bucket) {
            var dat = new[] {(byte) Signal.Codes.BucketGet};
            dat = dat.Concat(bucket.Guid.ToByteArray()).ToArray();

            var m = new Packet(Signal.Type.Control, dat).ToMsg();
            StorageBucket remoteBucket = null;
            m.OnResponse += (_, msg) => {
                var data = msg.Response.Data;
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch ((Signal.Codes) data[0]) {
                    case Signal.Codes.Ack:
                        var ser = new XmlSerializer(typeof(StorageBucket));
                        var str = new MemoryStream(data.Skip(1).ToArray());
                        remoteBucket = (StorageBucket) ser.Deserialize(str);
                        break;
                }
            };
            NetML.PostMessage(m);

            m.ResponseHandle.WaitOne();

            if (remoteBucket == null) throw new StorageBucketNotFoundException(bucket.Guid);

            bucket.CheckOnceForChanges(false);

            var res = new List<SimpleFileDiff>();
            var dealtWith = new Dictionary<string, FileData>();
            foreach (var l in bucket.SimpleChangeList) {
                if (!remoteBucket.SimpleChangeList.ContainsKey(l.Key)) {
                    res.Add(new SimpleFileDiff {
                        FileName = l.Key,
                        New = l.Value
                    });
                } else {
                    var loc = l.Value;
                    var rem = remoteBucket.SimpleChangeList[l.Key];
                    dealtWith.Add(l.Key, rem);

                    if (loc.Hash.Equals(rem.Hash)) continue; // no change
                    if (rem.ChangeTime > loc.ChangeTime)
                        res.Add(new SimpleFileDiff {
                            FileName = l.Key,
                            Old = loc,
                            New = rem
                        });
                    else
                        res.Add(new SimpleFileDiff {
                            FileName = l.Key,
                            Old = rem,
                            New = loc
                        });
                }
            }

            foreach (var r in remoteBucket.SimpleChangeList.Except(dealtWith)) {
                if (!bucket.SimpleChangeList.ContainsKey(r.Key)) {
                    res.Add(new SimpleFileDiff {
                        FileName = r.Key,
                        New = r.Value
                    });
                } else {
                    // this shouldn't happen...
                    Console.WriteLine("WTF!?");
                }
            }

            return res;
        }

        /// <summary>
        /// Gets the list of changed files from the remote and computes the differences.
        /// </summary>
        /// <param name="guid">The start of the buckets guid, formatted as returned by <see cref="T:System.Guid"/>.<see cref="M:System.Guid.ToString"/></param>
        /// <returns>The list of changes that have been found</returns>
        /// <exception cref="StorageBucketNotFoundException">If the bucket doesn't exist on the remote.</exception>
        public static List<SimpleFileDiff> GetSimpleChanges(string guid) {
            return GetSimpleChanges(GetBucketByGuid(guid));
        }

        #endregion

        #region sync

        public static void SendFile(StorageBucket bucket, string filename) {
            var fth = new FileTransferHeader {
                BucketGuid = bucket.Guid,
                ClientSend = true,
                Filename = filename
            };

            var (valid, index) = NetML.QueueFileTransfer(fth);
            if (!valid) throw new Exception("Invalid file transfer.");

            NetML.StartFileTransfer(index);
        }

        public static void DownloadFile(StorageBucket bucket, string filename) {
            var fth = new FileTransferHeader {
                BucketGuid = bucket.Guid,
                ServerSend = true,
                Filename = filename
            };

            var (valid, index) = NetML.QueueFileTransfer(fth);
            if (!valid) throw new Exception("Invalid file transfer.");

            NetML.StartFileTransfer(index);
        }

        #endregion
    }
}