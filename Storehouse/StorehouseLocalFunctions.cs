﻿using Storebase.net;
using Storebase.net.@base;
using Storebase.store;
using Storehouse.err;

namespace Storehouse {
    public static partial class StorehouseClientLibrary {
        /// <summary>
        /// Retrieves a bucket based on the start of it's guid
        /// </summary>
        /// <param name="guid">The start of the buckets guid, formatted as returned by <see cref="T:System.Guid"/>.<see cref="M:System.Guid.ToString"/></param>
        /// <returns>The bucket whose guid starts with <paramref name="guid"/>.</returns>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="AmbiguousGuidException">Raised when there are multiple buckets whose guid starts like <paramref name="guid"/>.</exception>
        public static StorageBucket GetBucketByGuid(string guid) {
            var (err, b) = Buckets.TryGetBucketWithGuidLike(guid);
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (err) {
                case Signal.Codes.ErrorBucketNotFound:
                    throw new StorageBucketNotFoundException(guid);
                case Signal.Codes.ErrorAmbiguousGuid:
                    throw new AmbiguousGuidException(guid);
            }

            return b;
        }

        /// <summary>
        /// Adds the specified <see cref="T:Storebase.store.StorageBucket"/> to the listing if it isn't listed yet.
        /// </summary>
        /// <param name="bucket">The bucket to add.</param>
        /// <param name="setActive">Whether or not to activate this bucket after adding it.</param>
        /// <param name="overrideActive">Whether or not to set the active bucket even when it is already set.</param>
        /// <param name="errorOnDuplicate">Whether or not to throw an exception when a bucket with the same guid as <paramref name="bucket"/> is already listed.</param>
        /// <exception cref="DuplicateStorageBucketException">Raised when <paramref name="errorOnDuplicate"/> is true and a bucket with the same guid as <paramref name="bucket"/> is already listed.</exception>
        public static void AddBucket(
            StorageBucket bucket, bool setActive = true, bool overrideActive = true, bool errorOnDuplicate = true) {
            if (!Buckets.Contains(bucket)) {
                Buckets.Add(bucket);
                if (setActive)
                    if (ActiveBucket == null || overrideActive)
                        SetActiveBucket(bucket);
            } else if (errorOnDuplicate) throw new DuplicateStorageBucketException(bucket.Guid);
        }

        /// <summary>
        /// Creates a <see cref="T:Storebase.store.StorageBucket"/> with the specified <paramref name="name"/> and <paramref name="path"/>.
        /// </summary>
        /// <param name="name">The name of the bucket.</param>
        /// <param name="path">The path to the bucket.</param>
        /// <returns>The <see cref="T:Storebase.store.StorageBucket"/> that was created.</returns>
        public static StorageBucket CreateAndAddBucket(string name, string path) {
            var b = new StorageBucket {
                Name = name,
                Path = path
            };

            AddBucket(b);

            return b;
        }

        /// <summary>
        /// Sets the <see cref="ActiveBucket"/> to <paramref name="bucket"/>.
        /// </summary>
        /// <param name="bucket">The bucket to activate.</param>
        /// <param name="addIfNotListed">Whether or not to list the bucket if it isn't already. Default is false.</param>
        /// <exception cref="StorageBucketUnlistedException">Raised when <paramref name="addIfNotListed"/> is false and the bucket is not listed.</exception>
        public static void SetActiveBucket(StorageBucket bucket, bool addIfNotListed = false) {
            if (addIfNotListed && !Buckets.Contains(bucket)) Buckets.Add(bucket);

            if (Buckets.Contains(bucket))
                ActiveBucket = bucket;
            else
                throw new StorageBucketUnlistedException(bucket);
        }

        /// <summary>
        /// Sets the <see cref="ActiveBucket"/> to <paramref name="bucket"/>.
        /// </summary>
        /// <param name="guid">The start of the buckets guid, formatted as returned by <see cref="T:System.Guid"/>.<see cref="M:System.Guid.ToString"/></param>
        /// <exception cref="StorageBucketNotFoundException">Raised when there is no bucket whose guid starts like <paramref name="guid"/>.</exception>
        /// <exception cref="AmbiguousGuidException">Raised when there are multiple buckets whose guid starts like <paramref name="guid"/>.</exception>
        public static void SetActiveBucket(string guid) => SetActiveBucket(GetBucketByGuid(guid));
    }
}