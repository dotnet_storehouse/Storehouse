﻿namespace Storehouse {
    public class StorehouseSettings {
        public string BucketCacheFile;
        public string CertificatePath;
    }
}