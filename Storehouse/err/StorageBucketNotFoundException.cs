﻿using System;

namespace Storehouse.err {
    public class StorageBucketNotFoundException : Exception {
        public StorageBucketNotFoundException() : this(
            "The requested storagebucket was not found. No further information was supplied.") { }

        public StorageBucketNotFoundException(string guidFragment) : base(
            "The requested storagebucket was not found. Guid fragment requested: '" + guidFragment + "'.") { }

        public StorageBucketNotFoundException(Guid guid) : base(
            "The requested storagebucket was not found. Guid requested: '" + guid + "'.") { }
    }
}