﻿using System;

namespace Storehouse.err {
    public class AmbiguousGuidException : Exception {
        public AmbiguousGuidException() : this(
            "A given guid fragment was ambiguous when it was required to be unique.") { }

        public AmbiguousGuidException(string guidFragment) : base(
            "The guid fragment '" + guidFragment + "' was ambiguous when it was required to be unique.") { }

        public AmbiguousGuidException(Guid guid) : base(
            "The guid '" + guid + "' was ambiguous when it was required to be unique.") { }
    }
}