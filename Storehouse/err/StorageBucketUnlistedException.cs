﻿using System;
using Storebase.store;

namespace Storehouse.err {
    public class StorageBucketUnlistedException : Exception {
        public StorageBucketUnlistedException(StorageBucket bucket) : base(
            "The bucket '" + bucket + "' was unlisted when it was expected to be.") { }
    }
}