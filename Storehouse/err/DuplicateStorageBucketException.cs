﻿using System;

namespace Storehouse.err {
    public class DuplicateStorageBucketException : Exception {
        public DuplicateStorageBucketException() : this(
            "The pushed or pulled storagebucket was already present on the target. No further information was supplied.") { }

        public DuplicateStorageBucketException(string guidFragment) : base(
            "The pushed or pulled storagebucket was already present on the target. Guid fragment requested: '" +
            guidFragment + "'.") { }

        public DuplicateStorageBucketException(Guid guid) : base(
            "The pushed or pulled storagebucket was already present on the target. Guid requested: '" +
            guid + "'.") { }
    }
}