﻿using System.Collections.Generic;
using Storebase.net.@base;
using Storebase.net.messageloop;
using Storebase.store;

namespace Storehouse {
    public class NetworkMessageLoop : StorehouseMessageLoop {
        internal static NetworkMessageLoop Instance;

        public NetworkMessageLoop(ITcpClient client) : base(client, false) {
            Instance = this;

            PostConstruct();
        }

        public override List<StorageBucket> GetBucketList() { return StorehouseClientLibrary.Buckets; }
    }
}